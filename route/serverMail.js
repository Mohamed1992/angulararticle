require('dotenv').config();
const nodemailer = require('nodemailer');
const express=require('express')
const router=express.Router()
//Step 1
let transporter = nodemailer.createTransport({
    service: 'gmail',
    host:'smtp.gmail.com',
    port: '465',
    ssl : true,
    auth:{
        user: process.env.EMAIL,
        pass: process.env.PASSWORD
    }
})


router.post('/email',async(req,res)=>{
   //step 2
  var message = " Nom :"+req.body.nom + "\r\n Email de client : "+req.body.email+"\r\n Sujet : "+req.body.subject+" \r\n Company : "+req.body.company+" \r\n Message : "+req.body.message;
  
  let mailOptions = {
       from: process.env.EMAIL,
       to: process.env.EMAIL,
       subject: req.body.subject,
       text: message
   }
   
   
   // Step  3
   transporter.sendMail(mailOptions, function(err,data){
       if(err){
         return res.send({status:false})
       }else{
         return res.send({status:true})
       }
   })
  
   
})

router.post('/emailValidation',async(req,res)=>{
 
  
  var message = " Bonjour :" +req.body.nom+ " \r\n Bienvenue Chez Errahma Stores \r\n SVP, validez votre compte par c'est url : "+req.body.url;
  
  console.log(message)
   let mailOptions = {
       from: process.env.EMAIL,
       to: req.body.email,
       subject: "Validation-Errahma-Stores",
       html: ` <div style='text-align:center;'> <span style="font-size:20px; padding:5px; font-weight:500; width:100%;"> Bonjour ${req.body.nom}! </span> <br> <span style="font-size:17px; padding:5px; font-weight:500; width:100%;"> Bienvenue Chez Errahma Stores </span> <br> <button> <a style='font-size:15px; padding:5px; font-weight:500;' href=${req.body.url} > Valider Votre Compte </a> </button> </div>`
   }
   
   console.log("ce bon 2")
   // Step  3
   transporter.sendMail(mailOptions, function(err,data){
       if(err){
         return res.send({status:false})
       }else{
         return res.send({status:true})
       }
   })
  
   

})

module.exports.routerServerMail=router
