const {Categorie} =require('../model/categorieModel')
const {Marque,validateMarque} =require('../model/marqueModel')
const express=require('express')
const router=express.Router()
const jwt = require('jsonwebtoken');


router.post('/newCategorie',  verifytoken, async(req,res)=>{
    if(req.user.user.role != "admin"){
        return res.status(401).send({status:false})
    }
    
    const categorie=new Categorie({
        nom:req.body.nom,
        categorie:req.body.categorie,
        image:req.body.image,
    },)
    
    const result=await categorie.save()
    return res.send({status:true,resultat:result})
})


router.get('/listCategories',async(req,res)=>{
  const result2=await Marque.find()
  const result=await Categorie.find()
  return res.send({status:true, resultat:result, marques:result2})
})

router.post('/updateCategorie/:id',  verifytoken, async(req,res)=>{
    if(req.user.user.role != "admin"){
        return res.status(401).send({status:false})
    }
    
    const result= await Categorie.findOneAndUpdate({_id:req.params.id},{ nom:req.body.nom, image:req.body.image})
    
    return res.send({status:true,resultat:result})
})



router.get('/deleteCategorie/:id',  verifytoken, async(req,res)=>{
    if(req.user.user.role != "admin"){
        return res.status(401).send({status:false})
    }
  
    if(await Categorie.findOneAndDelete({_id:req.params.id})){
        return res.send({status:true})
    }else{
        return res.send({status:false.err})
    }
})


function verifytoken(req, res, next){
  const bearerHeader = req.headers['authorization'];
  
  if(typeof bearerHeader !== 'undefined'){
 
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];
      jwt.verify(bearerToken, 'secretkey', (err, authData) => {
          if(err){
              res.sendStatus(403);
          }else{
              req.user = authData;
              next();
          }
      });
  
  }else{
    console.log("etape100");
     res.sendStatus(401);
  }

}

module.exports.routerCategorie=router