const {Chat} =require('../model/chatModel')
const {Message} =require('../model/messageModel')
const express=require('express')
const router=express.Router()
const jwt = require('jsonwebtoken');

router.post('/register', async(req,res)=>{
    
    const chat=new Chat({
        email:req.body.email,
        name:req.body.name,
        sexe:req.body.sexe,
    },)
    
    const result=await chat.save()
    return res.send({status:true,resultat:result})
})



router.get('/users', verifytoken,async(req,res)=>{
    if(req.user.user.role != "admin"){
        return res.status(401).send({status:false})
    }

    const result=await Chat.find()
    return res.send({status:true, resultat:result})
})

router.get('/numberMessages/:id', async(req,res)=>{
    
    const result=await Chat.find({_id:req.params.id})
    
    return res.send({status:true, resultat:result})
})



router.get('/userMessages/:id', async(req,res)=>{
   
    const result=await Message.find({idUser:req.params.id})

    return res.send({status:true, resultat:result})
})

router.post('/newMessage', async(req,res)=>{
    
    var datetime = new Date();
   
    const message=new Message({
        name:req.body.name,
        email:req.body.email,
        idUser: req.body.idUser,
        message: req.body.message,
        recepteur:"admin",
        date:datetime.toISOString().slice(0,10),
        heure:datetime.toISOString().slice(11,16),
    },)
    
    const result=await message.save()

    const resultat2 = await Chat.findById({_id:req.body.idUser})
    await Chat.findOneAndUpdate({_id:req.body.idUser},{ nomberMessageNonVueAdmin:resultat2.nomberMessageNonVueAdmin + 1 })
    
    return res.send({status:true,resultat:result})
})


router.post('/newMessageAdmin', verifytoken, async(req,res)=>{
    
    if(req.user.user.role != "admin"){
        return res.status(401).send({status:false})
    }

    const message=new Message({
        name:req.body.name,
        email:req.body.email,
        idUser: req.body.idUser,
        message: req.body.message,
        recepteur:"nonAdmin",
        date:"12/10/2020",
        heure:"14:14",
    },)

    const result=await message.save()

    const resultat2 = await Chat.findById({_id:req.body.idUser})
    await Chat.findOneAndUpdate({_id:req.body.idUser},{ nomberMessageNonVue:resultat2.nomberMessageNonVue + 1 })
    
    return res.send({status:true,resultat:result})
})


router.get('/renisialiserNumberUser/:id', async(req,res)=>{
   
    const result= await Chat.findOneAndUpdate({_id:req.params.id},{ nomberMessageNonVue:0 })
    
    return res.send({status:true,resultat:result})
})


router.get('/renisialiserNumberAdmin/:id', verifytoken, async(req,res)=>{
   
    if(req.user.user.role != "admin"){
        return res.status(401).send({status:false})
    }

    const result= await Chat.findOneAndUpdate({_id:req.params.id},{ nomberMessageNonVueAdmin:0 })
    
    return res.send({status:true,resultat:result})
})


function verifytoken(req, res, next){
  const bearerHeader = req.headers['authorization'];
  
  if(typeof bearerHeader !== 'undefined'){
 
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];
      jwt.verify(bearerToken, 'secretkey', (err, authData) => {
          if(err){
              res.sendStatus(403);
          }else{
              req.user = authData;
              next();
          }
      });
  
  }else{
    console.log("etape100");
    res.sendStatus(401);
  }

}

module.exports.routerChat=router