const {Article,validateArticle, validateArticlesCategories, validateModifiedArticle} =require('../model/aritcleModel')
const express=require('express')
const router=express.Router()
const jwt = require('jsonwebtoken');
const {User} =require('../model/userModel')
const {Marque} =require('../model/marqueModel')
const {Categorie} =require('../model/categorieModel')
var multer = require('multer');
const mongoose = require('mongoose');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + file.originalname)
    }
  })

  var upload = multer({ storage: storage })
  router.post('/upload',upload.array('myFiles'),async(req,res)=>{
    const files = req.files
    let arr=[];
  files.forEach(element => {
    
      arr.push("http://51.75.194.53:3000/"+element.path)
 
   })
   console.log(arr)
  return res.send(arr)
})

router.post('/listArticleByCategorie', async(req,res)=>{
  
  const options = {
    page: req.body.page,
    limit: 40,
    customLabels: myCustomLabels,
    sort:{
        createdAt: -1 
    }
  };

  const {error}=validateArticlesCategories(req.body)
  if(error) return res.status(400).send({status:false,message:error.details[0].message})
  
  var filter = {}
  
  if(req.body.marque != "0"){
    filter["nomMarque"] = req.body.marque   
  }


  filter["isEnabled"] = '1'

  if(req.body.sousSousCategorie != "0"){
     filter["sousSousCategorie"] = req.body.sousSousCategorie 
  }else if(req.body.sousCategorie != "0"){
    filter["sousCategorie"] = req.body.sousCategorie  
  }else{
    filter["categorie"] = req.body.categorie
  }

  if(req.body.maxPrice != 0){
     filter["$or"] = [{prix:{ $gte: req.body.minPrice , $lte: req.body.maxPrice }, isPromo:"0"}, {newPrix:{ $gte: req.body.minPrice , $lte: req.body.maxPrice }, isPromo:"1"}]    
  } 

  const result=await Article.paginate(filter, options)
  return res.send({status:true,resultat:result})


})

router.post('/cherche/:search', async(req,res)=>{
  
  const {error}=validateArticlesCategories(req.body)
  if(error) return res.status(400).send({status:false,message:error.details[0].message})
  
  const options = {
    page: req.body.page,
    limit: 40,
    customLabels: myCustomLabels
  };
  
  const result=await Article.paginate( { $or:[{nom:{$regex: '.*' + req.params.search + '.*' }}, {nomMarque:{$regex: '.*' + req.params.search + '.*' }}],isEnabled:"1"}, options) 
  return res.send({status:true,resultat:result}) 
  
})

router.get('/deleteArticle/:id',  verifytoken, async(req,res)=>{
    
    const article = await Article.findById(req.params.id)
    
    if(!article){
      return res.status(401).send({status:false})
    } 
 

  if(req.user.user.role != "admin"){
    
    if(req.user.user.id != article.proprietaire){
      return res.status(401).send({status:false})
    }
  }

  if(await Article.findOneAndDelete({_id:req.params.id})){
      return res.send({status:true})
  }else{
      return res.send({status:false.err})
  }
})


router.get('/test', async(req,res)=>{
 
  Article.paginate({}, options, function(err, result) {
    if (err) {
      return res.send(err);
    } else {
      return res.send(result);
    }
  });

})

const myCustomLabels = {
  totalDocs: 'itemCount',
  docs: 'itemsList',
  limit: 'perPage',
  page: 'currentPage',
  nextPage: 'next',
  prevPage: 'prev',
  totalPages: 'pageCount',
  pagingCounter: 'slNo',
  meta: 'paginator'
};
 

router.post('/listArticleByCategorieSearch/:search', verifytoken, async(req,res)=>{
  
  const {error}=validateArticlesCategories(req.body)
  if(error) return res.status(400).send({status:false,message:error.details[0].message})
  
  const options = {
    page: req.body.page,
    limit: 50,
    customLabels: myCustomLabels,
    sort:{
        createdAt: -1 
    }
  };
  

  if(req.user.user.role == "client"){
    return res.status(404).send({status:false})
  }

  var filter = {}
  
 if(req.body.marque != "0"){
    filter["nomMarque"] = req.body.marque   
  }

  filter["nom"] = { $regex: '.*' + req.params.search + '.*' } 

  if(req.user.user.role == "admin"){
        if(req.body.sousSousCategorie != "0"){
           filter["sousSousCategorie"] = req.body.sousSousCategorie 
        }else if(req.body.sousCategorie != "0"){
          filter["sousCategorie"] = req.body.sousCategorie  
        }else{
          filter["categorie"] = req.body.categorie
        }
  }else{
        filter["proprietaire"] = {_id:req.user.user.id}
        if(req.body.sousSousCategorie != "0"){
          filter["sousSousCategorie"] = req.body.sousSousCategorie  
        }else if(req.body.sousCategorie != "0"){
          filter["sousCategorie"] = req.body.sousCategorie  
        }else{
          filter["categorie"] = req.body.categorie
        }
  }

  if(req.body.maxPrice != 0){
     filter["$or"] = [{prix:{ $gte: req.body.minPrice , $lte: req.body.maxPrice }, isPromo:"0"}, {newPrix:{ $gte: req.body.minPrice , $lte: req.body.maxPrice }, isPromo:"1"}]    
  } 

  const result=await  Article.paginate(filter, options) 
  return res.send({status:true,resultat:result}) 
})


router.post('/listArticleByCategorieDashboard', verifytoken, async(req,res)=>{
  
  const options = {
    page: req.body.page,
    limit: 50,
    customLabels: myCustomLabels,
    sort:{
        createdAt: -1 
    }
  };

  const {error}=validateArticlesCategories(req.body)
  if(error) return res.status(400).send({status:false,message:error.details[0].message})
  
  if(req.user.user.role == "client"){
    return res.status(404).send({status:false})
  }
  
  var filter = {}
  
  if(req.body.marque != "0"){
    filter["nomMarque"] = req.body.marque   
  }
  
      
  if(req.user.user.role == "admin"){
        if(req.body.sousSousCategorie != "0"){
           filter["sousSousCategorie"] = req.body.sousSousCategorie 
        }else if(req.body.sousCategorie != "0"){
          filter["sousCategorie"] = req.body.sousCategorie  
        }else{
          filter["categorie"] = req.body.categorie
        }
  }else{
        filter["proprietaire"] = {_id:req.user.user.id}
        if(req.body.sousSousCategorie != "0"){
          filter["sousSousCategorie"] = req.body.sousSousCategorie  
        }else if(req.body.sousCategorie != "0"){
          filter["sousCategorie"] = req.body.sousCategorie  
        }else{
          filter["categorie"] = req.body.categorie
        }
  }

  if(req.body.maxPrice != 0){
     filter["$or"] = [{prix:{ $gte: req.body.minPrice , $lte: req.body.maxPrice }, isPromo:"0"}, {newPrix:{ $gte: req.body.minPrice , $lte: req.body.maxPrice }, isPromo:"1"}]    
  } 

  const result=await  Article.paginate(filter, options) 
  return res.send({status:true,resultat:result}) 
 
})

router.post('/modifierArticle/:id',  verifytoken, async(req,res)=>{

    const article = await Article.findById(req.params.id)
    
    if(!article){
      return res.status(401).send({status:false})
    } 

  if(req.user.user.role != "admin"){
    if(req.user.user.id != article.proprietaire){
      return res.status(401).send({status:false})
    }
  }

  if(req.body.nom == "undefined" || req.body.nom == "") return res.status(401).send({status:false});    
  if(req.body.prix == "undefined" || req.body.prix== "") return res.status(401).send({status:false});    
    
  var isPromo = "0"
  
  if(req.body.newPrix != 0){
     isPromo = "1"
  }

  var nomBoutique = "Errahma-stores"
  if(req.user.user.role != "admin"){
    nomBoutique = req.user.user.nomBoutique
  }
  
    const VerifyMarque = await Marque.findOne({nom: req.body.marque, categorie:req.body.categorie, sousCategorie:req.body.sousCategorie, sousSousCategorie:req.body.sousSousCategorie})
    if( VerifyMarque ){
          console.log("ce bon1")
    }else{

         var nomCategorie = "0"
         var nomSousCategorie = "0"
         var nomSousSousCategorie = "0"

        if(mongoose.Types.ObjectId.isValid(req.body.categorie)){
          const categorie1 = await Categorie.findById(req.body.categorie)
          if(categorie1){
             nomCategorie = categorie1.nom
          }
        }
 
       if(mongoose.Types.ObjectId.isValid(req.body.sousCategorie)){
         const categorie2 = await Categorie.findById(req.body.sousCategorie)
         if(categorie2){
           nomSousCategorie = categorie2.nom
         }
       }

       if(mongoose.Types.ObjectId.isValid(req.body.sousSousCategorie)){
         const categorie3 = await Categorie.findById(req.body.sousSousCategorie)
         if(categorie3){
           nomSousSousCategorie = categorie3.nom
         }
       }

    
         const marque2 =new Marque({
           nom:req.body.marque,
           categorie:req.body.categorie,
           sousCategorie:req.body.sousCategorie,
           sousSousCategorie:req.body.sousSousCategorie,
           nomCategorie:nomCategorie,
           nomSousCategorie:nomSousCategorie,
           nomSousSousCategorie:nomSousSousCategorie,
         },)
    
         const marque = await marque2.save()  

    }   

  const result = await Article.findOneAndUpdate({_id:req.params.id},{
      nom:req.body.nom,
      prix:req.body.prix,
      description:req.body.description,ref:req.body.ref,
      nomBoutique:nomBoutique,
      isPromo:isPromo,
      newPrix:req.body.newPrix,
      pourcentageRemise:req.body.pourcentageRemise,
      dateDebut:req.body.dateDebut,
      dateFin:req.body.dateFin,
      isPopulaires : req.body.isPopulaires,
      isTopCategorie : req.body.isTopCategorie,
      image : req.body.image,
      isFacilite:req.body.isFacilite,
      threeMonthe:req.body.threeMonthe,
      sexMonthe:req.body.sexMonthe,
      nineMonthe:req.body.nineMonthe,
      tweleveMonthe:req.body.tweleveMonthe,
      categorie:req.body.categorie,
      sousCategorie:req.body.sousCategorie,
      sousSousCategorie:req.body.sousSousCategorie,
       
  })

  return res.send({status:true,resultat:result})
})


router.post('/newArticle',  verifytoken, async(req,res)=>{
 
    if(req.user.user.role == "client"){
      return res.status(401).send({status:false})
    }
 
    if(req.body.nom == "undefined" || req.body.nom == "") return res.status(401).send({status:false});    
    if(req.body.prix == "undefined" || req.body.prix== "") return res.status(401).send({status:false});    
    if(req.body.image == "undefined" || req.body.image == "") return res.status(401).send({status:false});    
    if(req.body.marque == "undefined" || req.body.marque == "") return res.status(401).send({status:false});    
   

    const VerifyMarque = await Marque.findOne({nom: req.body.marque, categorie:req.body.categorie, sousCategorie:req.body.sousCategorie, sousSousCategorie:req.body.sousSousCategorie})
    if( VerifyMarque ){
          console.log("ce bon1")
    }else{

         var nomCategorie = "0"
         var nomSousCategorie = "0"
         var nomSousSousCategorie = "0"

        if(mongoose.Types.ObjectId.isValid(req.body.categorie)){
          const categorie1 = await Categorie.findById(req.body.categorie)
          if(categorie1){
             nomCategorie = categorie1.nom
          }
        }
 
       if(mongoose.Types.ObjectId.isValid(req.body.sousCategorie)){
         const categorie2 = await Categorie.findById(req.body.sousCategorie)
         if(categorie2){
           nomSousCategorie = categorie2.nom
         }
       }

       if(mongoose.Types.ObjectId.isValid(req.body.sousSousCategorie)){
         const categorie3 = await Categorie.findById(req.body.sousSousCategorie)
         if(categorie3){
           nomSousSousCategorie = categorie3.nom
         }
       }

    
         const marque2 =new Marque({
           nom:req.body.marque,
           categorie:req.body.categorie,
           sousCategorie:req.body.sousCategorie,
           sousSousCategorie:req.body.sousSousCategorie,
           nomCategorie:nomCategorie,
           nomSousCategorie:nomSousCategorie,
           nomSousSousCategorie:nomSousSousCategorie,
         },)
    
         const marque = await marque2.save()  

    }   

    var isPromo = "0"
    
    if(req.body.newPrix != 0){
       isPromo = "1"
    }

    var nomBoutique = "Errahma-store"
    if(req.user.user.role != "admin"){
      nomBoutique = req.user.user.nomBoutique
    }

    
    
    const article=new Article({
        nom:req.body.nom,
        prix:req.body.prix,
        image:req.body.image,
        categorie:req.body.categorie,
        sousCategorie:req.body.sousCategorie,
        sousSousCategorie:req.body.sousSousCategorie,
        description:req.body.description,
        nomMarque:req.body.marque,
        marque:req.body.marque,
        ref:req.body.ref,
        nomBoutique:nomBoutique,
        proprietaire:req.user.user.id,
        isPromo:isPromo,
        newPrix:req.body.newPrix,
        pourcentageRemise:req.body.pourcentageRemise,
        dateDebut:req.body.dateDebut,
        dateFin:req.body.dateFin,
    },)
    
    
    const result=await article.save()
    return res.send({status:true,resultat:result})
})



router.get('/articles/:id',async(req,res)=>{
  
  if(!mongoose.Types.ObjectId.isValid(req.params.id)){
       return res.status(400).json('articleId expected');
  }
    
  const result = await Article.findById(req.params.id)

  
  return res.send({status:true,resultat:result})

})

router.get('/admin/:id', verifytoken, async(req,res)=>{
  
     if(req.user.user.role != "admin"){
       return res.status(400).json('articleId expected');
    }


    if(!mongoose.Types.ObjectId.isValid(req.params.id)){
       return res.status(400).json('articleId expected');
    }
    
    const result = await Article.findById(req.params.id).populate('proprietaire')

    return res.send({status:true,resultat:result})
  })

router.get('/active/:id/:isEnabled', verifytoken, async(req,res)=>{
    
  if(req.user.user.role == "client"){
      return res.send({status:false})
  }
  
  if(!mongoose.Types.ObjectId.isValid(req.params.id)){
       return res.status(400).json('articleId expected');
  }
  
  if(req.user.user.role != "admin"){
    const result=await Article.findOneAndUpdate({id:req.params.id,proprietaire:{_id:req.user.user.id}},{isEnabled:req.params.isEnabled})
  }else{
    const result=await Article.findByIdAndUpdate(req.params.id,{isEnabled:req.params.isEnabled})
  }
  
  await res.send({status:true});
})

router.get('/accueil/_di',async(req,res)=>{
  const promos=await Article.find({isPromo:"1",isEnabled:"1"})
  const populaires=await Article.find({isPopulaires:"1",isEnabled:"1"})
  const topCategories=await Article.find({isTopCategorie:"1",isEnabled:"1"})
  return res.send({status:true, promos:promos, populaires:populaires, topCategories:topCategories})
})

router.get('/articleParCat',async(req,res)=>{
  const result=await Article.find({categorie:req.body.categorie})
  return res.send({status:true,resultat:result})
})

function verifytoken(req, res, next){
  const bearerHeader = req.headers['authorization'];
  
  if(typeof bearerHeader !== 'undefined'){
 
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];
      jwt.verify(bearerToken, 'secretkey', (err, authData) => {
          if(err){
              res.sendStatus(403);
          }else{
              req.user = authData;
              next();
          }
      });
  
  }else{
    console.log("etape100");
     res.sendStatus(401);
  }

}

module.exports.routerArticle=router