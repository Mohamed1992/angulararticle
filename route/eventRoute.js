const {Event,validateEvent} =require('../model/eventModel')
const express=require('express')
const router=express.Router()
const jwt = require('jsonwebtoken');

var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + file.originalname)
    }
  })

  var upload = multer({ storage: storage })

  router.post('/upload',upload.array('myFiles'),async(req,res)=>{
    const files = req.files
    let arr=[];
  files.forEach(element => {
    
      arr.push("http://51.75.194.53:3000/"+element.path)
 
   })
   console.log(arr)
  return res.send(arr)
})



router.post('/newEvent',  verifytoken, async(req,res)=>{
 
    if(req.user.user.role != "admin"){
      return res.status(401).send({status:false})
    }
 
    const {error}=validateEvent(req.body)
    if(error) return res.status(400).send({status:false,message:error.details[0].message})
    
    
    const event=new Event({
        titre:req.body.titre,
        description:req.body.description,
        image:req.body.image,
    },)
    
    
    const result=await event.save()
    return res.send({status:true,resultat:result})
})


router.get('/events',async(req,res)=>{
    const result=await Event.find()
    return res.send({status:true,resultat:result})
})

router.get('/supprimerEvent/:id', verifytoken, async(req,res)=>{
    
    if(req.user.user.role != "admin"){
        return res.status(401).send({status:false})
    }
  
    if(await Event.findOneAndDelete({_id:req.params.id})){
        return res.send({status:true})
    }else{
        return res.send({status:false})
    }
})


function verifytoken(req, res, next){
  const bearerHeader = req.headers['authorization'];
  
  if(typeof bearerHeader !== 'undefined'){
 
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];
      jwt.verify(bearerToken, 'secretkey', (err, authData) => {
          if(err){
              res.sendStatus(403);
          }else{
              req.user = authData;
              next();
          }
      });
  
  }else{
    console.log("etape100");
     res.sendStatus(401);
  }

}

module.exports.routerEvent=router