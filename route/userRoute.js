const {User,validateUser, validateLogin, validateBoutique, validateUpdateUser, validateUpdateBoutique} =require('../model/userModel')

const express=require('express')
const router=express.Router()
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


router.post('/admin',async(req,res)=>{

    const {error}=validateUser(req.body)
    if(error) return res.status(400).send({status:false,message:error.details[0].message})

    const emailExist = await User.findOne({ email: req.body.email});
    if(emailExist) return res.status(400).send('Email est déjà existé');

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    const admin = await User.findOne({ role: 'admin'});
 
    
    if (admin !== null){
        return res.send({status:false})
    }

    const user=new User({
        nom:req.body.nom,
        prenom:req.body.prenom,
        telephone:req.body.telephone,
        NCarteIdentite:req.body.NCarteIdentite,
        adresse:req.body.adresse,
        role:"admin",
        prenom:req.body.prenom,

        email:req.body.email,
        password: hashPassword,
    })

    const result=await user.save()
    return res.send({status:true})
})

const myCustomLabels = {
    totalDocs: 'itemCount',
    docs: 'itemsList',
    limit: 'perPage',
    page: 'currentPage',
    nextPage: 'next',
    prevPage: 'prev',
    totalPages: 'pageCount',
    pagingCounter: 'slNo',
    meta: 'paginator'
};

router.post('/register',async(req,res)=>{
    
    // const DeleteResultat = await User.findOneAndDelete({email:"rahma@gmail.com"})
     
    if(req.body.email == "undefined" || req.body.email == "") return res.status(400).send('email est vide');    
    
    if(req.body.password == "undefined" || req.body.password == "") return res.status(400).send('Le mot de passe est vide');    
   
    const emailExist = await User.findOne({ email: req.body.email});
    if(emailExist) return res.status(400).send('email est déjà existé');

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    let role = "boutique";
    
    if (req.body.role == "boutiqueWithCommandes"){
        role = "boutiqueWithCommandes";
    }if (req.body.role == "client"){
        role = "client";
    }

    const admin = await User.findOne({ role: 'admin'});
    
    if (admin == null){
        role = "admin"
    }

    var num = 0;
    if(role == "client"){
        const nbr = await User.count({role:'client'});
        num = nbr + 5000;
    }

    const user=new User({
        nom:req.body.nom,
        prenom:req.body.prenom,
        telephone:req.body.telephone,
        NCarteIdentite:req.body.NCarteIdentite,
        adresse:req.body.adresse,
        role:role,
        prenom:req.body.prenom,
        email:req.body.email,
        password: hashPassword,
        nomBoutique : req.body.nomBoutique,
        adressBoutique : req.body.adressBoutique,
        categorieBoutique : req.body.categorieBoutique,
        num:num,
        isValid: "1",
        ville: req.body.ville,
        pays: req.body.pays,
   
    })

    const result=await user.save()

   // const url = "http://localhost:4200/ValidEmail/" + result.id;

    jwt.sign({user}, 'secretkey', (err, token) => {
        res.json({
            token, role: result.role, status:true
        });
    });
   
    //return res.send({status:true,resultat:result})
})



router.post('/login',async(req,res)=>{
   
    const {error}=validateLogin(req.body)
    if(error) return res.status(400).send({status:false,message:error.details[0].message})

    const user = await User.findOne({ email: req.body.email});
    if(!user) return res.status(400).send('email or password is wrong');

    if(user.isValid == "0") return res.status(400).send('SVP, ouvrez votre boite gmail et validez votre compte');

    if(user.isEnabled == "0") return res.status(400).send('Votre compte est désactivé');

    const validPass = await bcrypt.compare(req.body.password, user.password);
    if(!validPass) return res.status(400).send('Invalid Mot de passe')    

    jwt.sign({user}, 'secretkey', (err, token) => {
        res.json({
            token, role: user.role
        });
    });
    

})


router.get('/validerCompte/:id', async(req,res)=>{
  
     
        const user = await User.findById(req.params.id)

        if(user){
            const result = await User.findByIdAndUpdate(user.id,{
                isValid:"1",
             })

             return res.send({status:true,resultat:result})

        }
    
        return res.send({status:false})
})


router.get('/:id', verifytoken, async(req,res)=>{
  
    if(!req.params.id && req.user.user.role != "admin")
    return res.status(400).json('articleId expected');
    
    const result=await User.findById(req.params.id)
    
    return res.send({status:true,resultat:result})
})

router.get('/details/_id', verifytoken, async(req,res)=>{
  const result = await User.findById(req.user.user.id)
 
   result.password = ""
  return res.send({status:true,resultat:result})
})

router.post('/update/_id', verifytoken, async(req,res)=>{

    if(req.body.email == "undefined" || req.body.email == "") return res.status(400).send('email est vide');    
    
    if(req.body.password == "undefined" || req.body.password == "") return res.status(400).send('Le mot de passe est vide');    
  
   
    
    const user = await User.findById(req.user.user.id)

    if(user.password == "undefined" || user.password == "") return res.status(400).send('Votre compte ne existe pas');

    var isValid = "1"
    
    if(req.body.email != user.email){
     //  isValid = "0"   
       const emailExist = await User.count({ email: req.body.email});
       if(emailExist > 0) return res.status(400).send('email est déjà existé');
    }

    var hashPassword = ""
    if(req.body.newPassword == "undefined" || req.body.newPassword == ""){
       hashPassword = user.password
    }else{
       const salt = await bcrypt.genSalt(10);
       hashPassword = await bcrypt.hash(req.body.newPassword, salt);
    }
    
    const validationPassword = await bcrypt.compare(req.body.password, user.password)
    
    if(!validationPassword){
        return res.send({status:false})
    }

    if(req.user.user.role == "admin" && req.user.user.role == "client"){
        const result = await User.findByIdAndUpdate(req.user.user.id,{
           nom:req.body.nom,
           prenom:req.body.prenom,
           telephone:req.body.telephone,
           NCarteIdentite:req.body.NCarteIdentite,
           adresse:req.body.adresse,
           prenom:req.body.prenom,
           email:req.body.email,
           password: hashPassword,
           isValid:isValid,
           ville: req.body.ville,
           pays: req.body.pays,
        })
        
        const url = "http://localhost:4200/ValidEmail/" + result.id;
        return res.send({status:true,resultat:result, url:url, isValid:isValid})

    }else{
        const result = await User.findByIdAndUpdate(req.user.user.id,{
            nom:req.body.nom,
            prenom:req.body.prenom,
            telephone:req.body.telephone,
            NCarteIdentite:req.body.NCarteIdentite,
            adresse:req.body.adresse,
            prenom:req.body.prenom,
            email:req.body.email,
            nomBoutique : req.body.nomBoutique,
            adressBoutique : req.body.adressBoutique,
            categorieBoutique : req.body.categorieBoutique,
            password: hashPassword,
            isValid:isValid,
            ville: req.body.ville,
            pays: req.body.pays,
         })
         const url = "http://localhost:4200/ValidEmail/" + result.id;
         return res.send({status:true,resultat:result, url:url, isValid:isValid})
    }

   
    
        
})


router.post('/listesBoutiques', verifytoken, async(req,res)=>{
    const options = {
        page: req.body.page,
        limit: 5,
        customLabels: myCustomLabels
    };
  
   if(req.user.user.role != "admin"){
     return res.send({status:false})
   }

   const result=await User.paginate({$or: [{role:"boutique"}, {role:"boutiqueWithCommandes"}]}, options)
   return res.send({status:true,resultat:result})
})

router.post('/listesClients', verifytoken, async(req,res)=>{

    const options = {
        page: req.body.page,
        limit: 5,
        customLabels: myCustomLabels
    };
    
    if(req.user.user.role != "admin"){
      return res.send({status:false})
    }
    const result=await User.paginate({role:"client"}, options)
    
    return res.send({status:true,resultat:result})
 })

 



router.get('/active/:id/:isEnabled', verifytoken, async(req,res)=>{
    
    if(req.user.user.role != "admin"){
        return res.send({status:false})
    }
    
    const result=await User.findByIdAndUpdate(req.params.id,{isEnabled:req.params.isEnabled})
      
    await res.send({status:true});
})


function verifytoken(req, res, next){
    const bearerHeader = req.headers['authorization'];
      
    if(typeof bearerHeader !== 'undefined'){
    
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];

        jwt.verify(bearerToken, 'secretkey', (err, authData) => {
            if(err){
                res.sendStatus(403);
            }else{
                req.user = authData;
                next();
            }
        });
    
    }else{
        res.sendStatus(401);
    }

}

module.exports.verifytoken=verifytoken
module.exports.routerUser=router