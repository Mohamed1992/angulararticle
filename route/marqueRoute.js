const {Marque,validateMarque} =require('../model/marqueModel')
const {Categorie} =require('../model/categorieModel')
const express=require('express')
const router=express.Router()
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const myCustomLabels = {
    totalDocs: 'itemCount',
    docs: 'itemsList',
    limit: 'perPage',
    page: 'currentPage',
    nextPage: 'next',
    prevPage: 'prev',
    totalPages: 'pageCount',
    pagingCounter: 'slNo',
    meta: 'paginator'
  };



router.post('/newMarque',  verifytoken, async(req,res)=>{

    if(req.user.user.role != "admin"){
        return res.status(401).send({status:false})
      }
    
    const {error}=validateMarque(req.body)
    if(error) return res.status(400).send(error.details[0].message)


    const VerifyMarque = await Marque.findOne({nom: req.body.nom, categorie:req.body.categorie, sousCategorie:req.body.sousCategorie, sousSousCategorie:req.body.sousSousCategorie})
    
    if( VerifyMarque ) return res.status(400).send('Votre marque est deja existe')  

    var nomCategorie = "0"
    var nomSousCategorie = "0"
    var nomSousSousCategorie = "0"

    if(mongoose.Types.ObjectId.isValid(req.body.categorie)){
       const categorie1 = await Categorie.findById(req.body.categorie)
       if(categorie1){
         nomCategorie = categorie1.nom
       }
    }

    if(mongoose.Types.ObjectId.isValid(req.body.sousCategorie)){
       const categorie2 = await Categorie.findById(req.body.sousCategorie)
       if(categorie2){
         nomSousCategorie = categorie2.nom
       }
    }

    if(mongoose.Types.ObjectId.isValid(req.body.sousSousCategorie)){
       const categorie3 = await Categorie.findById(req.body.sousSousCategorie)
       if(categorie3){
         nomSousSousCategorie = categorie3.nom
       }
    }

    const marque=new Marque({
        nom:req.body.nom,
        categorie:req.body.categorie,
        sousCategorie:req.body.sousCategorie,
        sousSousCategorie:req.body.sousSousCategorie,
        nomCategorie:nomCategorie,
        nomSousCategorie:nomSousCategorie,
        nomSousSousCategorie:nomSousSousCategorie,
    },)
    
    const result=await marque.save()
    return res.send({status:true,resultat:result})
})


router.post('/updateMarque/:id',  verifytoken, async(req,res)=>{
    if(req.user.user.role != "admin"){
        return res.status(401).send({status:false})
      }
    
    const result= await Marque.findOneAndUpdate({_id:req.params.id},{ nom:req.body.nom})
    
    return res.send({status:true,resultat:result})
})



router.get('/deleteMarque/:id',  verifytoken, async(req,res)=>{
    if(req.user.user.role != "admin"){
        return res.status(401).send({status:false})
    }
  
    if(await Marque.findOneAndDelete({_id:req.params.id})){
        return res.send({status:true})
    }else{
        return res.send({status:false.err})
    }
})

router.get('/listMarque', async(req,res)=>{

  const result=await Marque.find()
  return res.send({status:true,resultat:result})

})


function verifytoken(req, res, next){
  const bearerHeader = req.headers['authorization'];
  
  if(typeof bearerHeader !== 'undefined'){
 
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];
      jwt.verify(bearerToken, 'secretkey', (err, authData) => {
          if(err){
              res.sendStatus(403);
          }else{
              req.user = authData;
              next();
          }
      });
  
  }else{
     res.sendStatus(401);
  }

}

module.exports.routerMarque=router