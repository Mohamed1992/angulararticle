const {Commande,validateCommande} =require('../model/commandeModel')
const {Chat} =require('../model/chatModel')
const express=require('express')
const router=express.Router()
const jwt = require('jsonwebtoken');


router.post('/newCommande',  verifytoken, async(req,res)=>{
    const {error}=validateCommande(req.body)
    if(error) return res.status(400).send({status:false,message:error.details[0].message})
    
    if(req.user.user.role != "client"){
        return res.status(403).send({status:false})
    }

    const nbr = await Commande.count({});
    const num = nbr + 1;
    
    const commande=new Commande({
        client:req.user.user.id,
        commande:req.body,
        etat:"enAttent",
        num:num,
    },)

    
    const result=await commande.save()
    return res.send({status:true,resultat:result})
})

const myCustomLabels = {
    totalDocs: 'itemCount',
    docs: 'itemsList',
    limit: 'perPage',
    page: 'currentPage',
    nextPage: 'next',
    prevPage: 'prev',
    totalPages: 'pageCount',
    pagingCounter: 'slNo',
    meta: 'paginator'
  };

router.get('/modifierEtat/:idCommande/:etat', verifytoken, async(req,res)=>{
  
    var etat = req.params.etat
    
    const etats = ["enAttent","accepte","refuse","fini"]
    
    var ok = false
    
    for(i = 0; i < etats.length; i++){
        if(etat == etats[i] ){
            ok = true
        }
    }

    if(!ok){
        return res.status(400).send({status:false})
    }

    if(req.user.user.role == "admin" ){
        const result=await Commande.findByIdAndUpdate(req.params.idCommande,{etat:etat})
        return res.send({status:true,resultat:result})
    }

    return res.status(404).send({status:false})
})

router.get('/showAdmin/:id', verifytoken, async(req,res)=>{
    
  if(req.user.user.role != "admin"){
      return res.send({status:false})
  }
  
  const result=await Commande.findOneAndUpdate({_id:req.params.id},{isShowAdmin:1})
  
  await res.send({status:true});
})

router.get('/newCommandesMessages', verifytoken, async(req,res)=>{
    
  if(req.user.user.role != "admin"){
      return res.send({status:false})
  }
  
  const nbr = await Commande.count({isShowAdmin:0, etat:"enAttent"});
  
  const nbr2 = await Chat.aggregate([ { $group: { _id: null, nomberMessageNonVueAdmin: { $sum: "$nomberMessageNonVueAdmin" } } }])
  
  await res.send({status:true,nbrCommandes:nbr, nbrMessages:nbr2});
})


router.post('/listCommandes/:etat', verifytoken, async(req,res)=>{
  
   
    const options = {
        page: req.body.page,
        limit: 20,
        customLabels: myCustomLabels,
        populate: 'client',
        sort:{
           createdAt: -1 
        }
    };

    var etat = req.params.etat
    const etats = ["enAttent","accepte","refuse","fini"]

    var ok = false
    
    for(i = 0; i < etats.length; i++){
        if(etat == etats[i] ){
            ok = true
        }
    }

    if(!ok){
        return res.status(400).send({status:false})
    }

    if(req.user.user.role == "admin"){
        const result=await Commande.paginate({etat:etat}, options)
        return res.send({status:true,resultat:result})
    }else if(req.user.user.role == "client"){
        const result=await Commande.paginate({etat:etat, client:req.user.user.id}, options)
        return res.send({status:true,resultat:result})
    }else if(req.user.user.role == "boutiqueWithCommandes"){
        const result=await Commande.paginate({etat:etat, 'commande.proprietaire':req.user.user.id}, options)
        return res.send({status:true,resultat:result})
    }    
    
})


function verifytoken(req, res, next){
  const bearerHeader = req.headers['authorization'];
  
  if(typeof bearerHeader !== 'undefined'){
 
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];
      jwt.verify(bearerToken, 'secretkey', (err, authData) => {
          if(err){
              res.sendStatus(403);
          }else{
              req.user = authData;
              next();
          }
      });
  
  }else{
     res.sendStatus(401);
  }

}

module.exports.routerCommande=router