const mongoose=require('mongoose')
const Joi=require('joi')
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema

const schemaEvent=mongoose.Schema({
    titre:{type:String,required:true},
    description:{type:String,required:true},
    image:{type:String,required:true},
},
{ timestamps: true }
)

schemaEvent.plugin(mongoosePaginate);

schemaEvent.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });



const Event=mongoose.model('Event',schemaEvent)

function validateEvent(event){

    const schema={
        titre:Joi.string().required(),
        description:Joi.string().required(),
        image:Joi.string().required(),
    }

    return Joi.validate(event,schema)
}

module.exports.Event=Event
module.exports.validateEvent=validateEvent