const mongoose=require('mongoose')
const Joi=require('joi')
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema

const schemaCategorie=mongoose.Schema({
    
    nom:{type:String,required:true},
    categorie:{type:String,required:true},
},
{ timestamps: true }
)

schemaCategorie.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  schemaCategorie.plugin(mongoosePaginate);

const Categorie=mongoose.model('Categorie',schemaCategorie)

function validateCategorie(categorie){

    const schema={
        nom:Joi.string().required(),
        categorie:Joi.string().required(),
    }

    return Joi.validate(categorie,schema)
}



module.exports.Categorie=Categorie
