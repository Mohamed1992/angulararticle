const mongoose=require('mongoose')
const Joi=require('joi')
const mongoosePaginate = require('mongoose-paginate');

const schemaUser=mongoose.Schema({
    
    nom: {type: String, default: ""},
    prenom: {type: String, default: ""},
    password: {type: String, default: ""},
    email: {type: String, default: "", unique: true},
    telephone: {type: String, default: ""},
    NCarteIdentite: {type: String, default: " "},
    adresse: {type: String, default: " "},
    ville: {type: String, default: " "},
    pays: {type: String, default: " "},
    role:{type: String, default: "client"},
    num:{type:Number,required:true},
    nomBoutique : {type: String, default: ""},
    adressBoutique : {type: String, default: ""},
    categorieBoutique : {type: String, default: ""},

    isEnabled:{type: String, default: "1"},
    isValid:{type: String, default: "1"}
    }
    ,
    { timestamps: true },
)

schemaUser.plugin(mongoosePaginate);

  schemaUser.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });


const User=mongoose.model('User',schemaUser)

function validateUser(user){

    
    const schema={
        nom:Joi.string().required(),
        prenom:Joi.string().required(),
        telephone:Joi.string().required(),
        adresse:Joi.string().required(),
        NCarteIdentite:Joi.string().required(),
        email:Joi.string().min(6).required().email(),
        password:Joi.string().required(),
        role:Joi.string().required(),
    }

    return Joi.validate(user,schema)
}

function validateBoutique(user){

    
    const schema={
        nom:Joi.string().required(),
        prenom:Joi.string().required(),
        telephone:Joi.string().required(),
        adresse:Joi.string().required(),
        NCarteIdentite:Joi.string().required(),
        email:Joi.string().min(6).required().email(),
        nomBoutique : Joi.string().min(1).required(),
        password:Joi.string().min(6).required(),
        adressBoutique : Joi.string().min(1).required(),
        categorieBoutique : Joi.string().min(1).required(),
        role:Joi.string().required(),
    
    }

    return Joi.validate(user,schema)
}

function validateUpdateUser(user){

    
    const schema={
        nom:Joi.string().required(),
        prenom:Joi.string().required(),
        telephone:Joi.string().required(),
        adresse:Joi.string().required(),
        NCarteIdentite:Joi.string().required(),
        email:Joi.string().min(6).required().email(),
        password:Joi.string().required(),
        newPassword:Joi.string().required(),
        
    }

    return Joi.validate(user,schema)
}


function validateUpdateBoutique(user){

    
    const schema={
        nom:Joi.string().required(),
        prenom:Joi.string().required(),
        telephone:Joi.string().required(),
        adresse:Joi.string().required(),
        NCarteIdentite:Joi.string().required(),
        nomBoutique : Joi.string().min(1).required(),
        password:Joi.string().min(6).required(),
        adressBoutique : Joi.string().min(1).required(),
        categorieBoutique : Joi.string().min(1).required(),
        newPassword:Joi.string().required(),        
    
    }

    return Joi.validate(user,schema)
}


function validateLogin(login){

    const schema={
        email:Joi.string().required().email(),
        password:Joi.string().required()
    }

    return Joi.validate(login,schema)
}



module.exports.User=User
module.exports.validateLogin=validateLogin
module.exports.validateUser=validateUser
module.exports.validateBoutique=validateBoutique
module.exports.validateUpdateUser=validateUpdateUser
module.exports.validateUpdateBoutique=validateUpdateBoutique