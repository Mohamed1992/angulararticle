const mongoose=require('mongoose')
const Joi=require('joi')
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema

const schemaArticle=mongoose.Schema({
    
    nom:{type:String, default: "Errahma-stores"},
    prix:{type:Number, default: 00},
    image:{type:String, default: ""},
    categorie:{type:String, default: ""},
    sousCategorie:{type:String, default: ""},
    sousSousCategorie:{type:String, default: ""},
    description:{type:String, default: ""},
    marque:{type:String,default: "Autres"},
    nomMarque:{type:String},
    ref:{type:String, default: ""},
    nomBoutique:{type:String,default: "Errahma-stores"},
    proprietaire:{type: Schema.Types.ObjectId, ref: "User"},
    isEnabled:{type: String, default: "1"},
    isPromo:{type: String, default: "0"},
    newPrix:{type:Number,default: "0"},
    pourcentageRemise:{type:Number,default: "0"},
    dateDebut:{type:String,default: "0"},
    dateFin:{type:String,default: "0"},
    isPromo:{type: String, default: "0"},
    isPopulaires:{type: String, default: "0"},
    isTopCategorie:{type: String, default: "0"},
    imageArrieur:{type:String,default: ""},
    isFacilite:{type: String, default: "0"},
    threeMonthe:{type:Number,default: 0},
    sexMonthe:{type:Number,default: 0},
    nineMonthe:{type:Number,default: 0},
    tweleveMonthe:{type:Number,default: 0},
    
},
{ timestamps: true }
)

schemaArticle.plugin(mongoosePaginate);

schemaArticle.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });



const Article=mongoose.model('Article',schemaArticle)

function validateArticle(article){

    const schema={
        nom:Joi.string().required(),
        prix:Joi.number().required(),
        image:Joi.string().required(),
        categorie:Joi.string().required(),
        sousCategorie:Joi.string().required(),
        sousSousCategorie:Joi.string().required(),
        description:Joi.string(),
        marque:Joi.string(),
        qtn:Joi.number().required(),
        ref:Joi.string().required(),
        newPrix:Joi.number(),
        pourcentageRemise:Joi.number(),
        dateDebut:Joi.string(),
        dateFin:Joi.string(),
    
    }

    return Joi.validate(article,schema)
}

function validateModifiedArticle(article){

    const schema={
        nom:Joi.string().required(),
        prix:Joi.number().required(),
        description:Joi.string(),
        marque:Joi.string(),
        qtn:Joi.number().required(),
        ref:Joi.string().required(),
        newPrix:Joi.number(),
        pourcentageRemise:Joi.number(),
        dateDebut:Joi.string(),
        dateFin:Joi.string(),
        isPopulaires:Joi.string(),
        imageArrieur:Joi.string().allow(null, ''),
        isTopCategorie:Joi.string(),
    }

    return Joi.validate(article,schema)
}


function validateArticlesCategories(categorie){

    const schema={
        categorie:Joi.string().required(),
        sousCategorie:Joi.string().required(),
        sousSousCategorie:Joi.string().required(),
        page:Joi.number().required(),
        minPrice:Joi.number().allow(0),
        maxPrice:Joi.number().allow(0),
        marque:Joi.string().required(),
    }

    return Joi.validate(categorie,schema)
}

module.exports.Article=Article
module.exports.validateArticle=validateArticle
module.exports.validateArticlesCategories=validateArticlesCategories
module.exports.validateModifiedArticle=validateModifiedArticle