const mongoose=require('mongoose')
const Joi=require('joi')
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema

const schemaChat=mongoose.Schema({
    name: {type: String, default: ""},
    email: {type: String, default: ""},
    nomberMessageNonVue:{type:Number, default: 0},
    nomberMessageNonVueAdmin:{type:Number, default: 0},
    sexe:{type:Number, default: 0},
},
{ timestamps: true }
)

schemaChat.plugin(mongoosePaginate);

schemaChat.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });



const Chat=mongoose.model('Chat',schemaChat)



module.exports.Chat=Chat