const mongoose=require('mongoose')
const Joi=require('joi')
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema

const schemaCommande=mongoose.Schema({
    
    commande:[{
        quantite:{type:String,required:true},
        ref:{type:String,default: ""},
        nom:{type:String,required:true},
        prix:{type:Number,required:true},
        isPromo:{type:String, default: "0"},
        newPrix:{type: Number, default: 0},
        image:{type:String,required:true},
        proprietaire:{type:String,required:true},
    }],
    client:{type: Schema.Types.ObjectId, ref: "User"},
    etat:{type:String,required:true},
    num:{type:Number,required:true},
    isShowAdmin:{type:Number,default: 0},
},
{ timestamps: true }
)

schemaCommande.plugin(mongoosePaginate);

schemaCommande.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

const Commande = mongoose.model('Commande',schemaCommande)


function validateCommande(commande){
    
    let item = Joi.object().keys({
        quantite:Joi.string().required(),
        ref:Joi.string().allow(""),
        nom:Joi.string().required(),
        prix:Joi.number().required(),
        newPrix:Joi.number().required(),
        isPromo:Joi.string().required(),
        image:Joi.string().required(),
        proprietaire:Joi.string().required(),
    })
      
    let schema = Joi.array().items(item)
      
    return Joi.validate(commande,schema)
}

module.exports.Commande=Commande
module.exports.validateCommande=validateCommande