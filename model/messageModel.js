const mongoose=require('mongoose')
const Joi=require('joi')
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema

const schemaMessage=mongoose.Schema({
    idUser: {type: String, default: ""},
    email: {type: String, default: ""},
    name: {type: String, default: ""},
    recepteur:{type: String, default: ""},
    isVue:{type: String, default: "0"},
    message:{type: String, default: ""},
    date:{type: String, default: ""},
    heure:{type: String, default: ""},
},
{ timestamps: true }
)

schemaMessage.plugin(mongoosePaginate);

schemaMessage.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });



const Message=mongoose.model('Message',schemaMessage)



module.exports.Message=Message