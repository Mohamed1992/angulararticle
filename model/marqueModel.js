const mongoose=require('mongoose')
const Joi=require('joi')
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema

const schemaMarque=mongoose.Schema({
    
    nom:{type:String,required:true},
    nomCategorie:{type:String, default: ""},
    nomSousCategorie:{type:String, default: ""},
    nomSousSousCategorie:{type:String, default: ""},
    categorie:{type:String, default: ""},
    sousCategorie:{type:String, default: ""},
    sousSousCategorie:{type:String, default: ""},
},
{ timestamps: true }
)

schemaMarque.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  schemaMarque.plugin(mongoosePaginate);

const Marque=mongoose.model('Marque',schemaMarque)

function validateMarque(marque){

    const schema={
        nom:Joi.string().required(),
        categorie:Joi.string().required(),
        sousCategorie:Joi.string().required(),
        sousSousCategorie:Joi.string().required(),
    }

    return Joi.validate(marque,schema)
}

module.exports.Marque=Marque
module.exports.validateMarque=validateMarque