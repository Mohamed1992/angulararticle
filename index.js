const express=require("express")
const https = require('https');
const path = require('path')
const fs = require('fs')
const app=express()
const bodyParser=require('body-parser')
const mongoose=require("mongoose")

const {routerMarque} = require("./route/marqueRoute")
const {routerArticle} = require("./route/articleRoute")
const {routerUser} = require("./route/userRoute")
const {routerCategorie} = require("./route/categorieRoute")
const {routerCommande} = require("./route/commandeRoute")
const {routerEvent} = require("./route/eventRoute")
const {routerServerMail} = require("./route/serverMail")
const {routerChat} = require("./route/chatRoute")

const cors=require('cors')
mongoose.connect("mongodb://localhost/shopBD",{ useUnifiedTopology: true,useNewUrlParser: true })
.then(console.log("connected to mongodb"))
.catch(err=>console.log(err))

app.use(express.json())

app.use(cors())

app.use('/marque',routerMarque)
app.use('/article',routerArticle)
app.use('/user',routerUser)
app.use('/categorie',routerCategorie)
app.use('/commande',routerCommande)
app.use('/event',routerEvent)
app.use('/contact',routerServerMail)
app.use('/chat',routerChat)

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('uploads'));
app.use('/uploads', express.static(__dirname + '/uploads/'));


const server = require('http').createServer(app)

var io = require('socket.io')(server, { origins: '*:*'});


io.on('connection', (socket) => {
    socket.on("chatErrahmaStore", data => {
        socket.broadcast.emit('chatErrahmaStore',data)

        // socket.emit return to this socket
        // io.emit  return all sockets connected
        // socket.broadcast.emit return all sockets connected broadcast this

    });

    socket.on("newRegister", data => {
        socket.broadcast.emit('newRegister',data)
    });

    socket.on("newVueMessage", data => {
        socket.broadcast.emit('newVueMessage',data)
    });

})

server.listen(3000, () => console.log("server listen on port 3000"));